# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

stages:
  - build

.workspace:
  interruptible: true
  image:
    name: registry.ostc-eu.org/ostc/oniro/bitbake-builder:latest
  tags: [large-disk, $CI_ONIRO_RUNNER_TAG, $CI_ONIRO_INSTANCE_SIZE]
  variables:
    CI_ONIRO_RUNNER_TAG : ""
    CI_ONIRO_MANIFEST_URL: https://gitlab.eclipse.org/eclipse/oniro-core/oniro
    CI_ONIRO_MANIFEST_BRANCH: kirkstone
    CI_ONIRO_MANIFEST_NAME: default.xml
    CI_ONIRO_MANIFEST_MIRROR: oniro-develop
    CI_ONIRO_GIT_REPO_PATH: ""
    CI_ONIRO_INSTANCE_SIZE: s3.large.8
  before_script:
    - test ! -e "$CI_PROJECT_DIR"/.scratch-dir-name || (
        echo "precondition failed - concurrent modification of $CI_PROJECT_DIR"
        && env | grep CI_ | sort
        && ls -l
        && exit 1 )

    # Create scratch space, being careful not to pollute the working directory.
    # This is done so that we are not attached to the contents of
    # $CI_PROJECT_DIR which contains something that GitLab CI prepared for us.
    - SCRATCH_DIR="$(mktemp -p /tmp -d workspace.XXXXXXXXXX)"
    - echo "$SCRATCH_DIR" > "$CI_PROJECT_DIR"/.scratch-dir-name
    - cd "$SCRATCH_DIR"
    - |
        set -x
        echo "Building repo workspace with the following properties:"
        echo "CI_ONIRO_MANIFEST_URL: $CI_ONIRO_MANIFEST_URL"
        echo "CI_ONIRO_MANIFEST_NAME: $CI_ONIRO_MANIFEST_NAME"
        echo "CI_ONIRO_MANIFEST_BRANCH: $CI_ONIRO_MANIFEST_BRANCH"
        repo init \
           $(test -n "${CI_ONIRO_RUNNER_PERSISTENT_STORAGE:-}" && echo --reference "$CI_ONIRO_RUNNER_PERSISTENT_STORAGE"/pub/git-repo-mirrors/"$CI_ONIRO_MANIFEST_MIRROR") \
           --manifest-url "$CI_ONIRO_MANIFEST_URL" \
           --manifest-name "$CI_ONIRO_MANIFEST_NAME" \
           --manifest-branch "$CI_ONIRO_MANIFEST_BRANCH"
        set +x
    - time repo sync --no-clone-bundle

  script:
    # Reload the value of SCRATCH_DIR set in the before_script phase. Those run
    # in separate shell processes and do not share environment variables.
    - SCRATCH_DIR="$(cat "$CI_PROJECT_DIR"/.scratch-dir-name)"
    - cd "$SCRATCH_DIR"

# This job is documented in docs/ci/hidden-jobs/bitbake-workspace.rst
.bitbake-workspace:
  extends: .workspace
  stage: build
  timeout: 10h
  variables:
    CI_ONIRO_BUILD_CACHE: "private"
    CI_ONIRO_BB_LOCAL_CONF_DL_DIR: $CI_ONIRO_RUNNER_PERSISTENT_STORAGE/$CI_ONIRO_BUILD_CACHE/bitbake/downloads
    CI_ONIRO_BB_LOCAL_CONF_SSTATE_DIR: $CI_ONIRO_RUNNER_PERSISTENT_STORAGE/$CI_ONIRO_BUILD_CACHE/bitbake/sstate-cache

  before_script:
    # Bitbake requires a non-root user to operate.
    # The container should have a non-root user by default.
    - test "$(id -u)" -ne 0 || (
        echo "precondition failed - this job cannot run as root"
        && exit 1 )
    # Bitbake is configured to use $CI_ONIRO_RUNNER_PERSISTENT_STORAGE/$CI_ONIRO_BUILD_CACHE/bitbake
    # directory for both the download directory and the sstate-cache.
    - test -n "$CI_ONIRO_RUNNER_PERSISTENT_STORAGE" || (
        echo "precondition failed - CI_ONIRO_RUNNER_PERSISTENT_STORAGE is not set"
        && exit 1 )
    - test -w "$CI_ONIRO_RUNNER_PERSISTENT_STORAGE/$CI_ONIRO_BUILD_CACHE/bitbake" || (
        echo "precondition failed - expected $CI_ONIRO_RUNNER_PERSISTENT_STORAGE/$CI_ONIRO_BUILD_CACHE/bitbake to be writable"
        && exit 1 )
    # Log available disk space on the persistent shared disk.
    - df -h "$CI_ONIRO_RUNNER_PERSISTENT_STORAGE/$CI_ONIRO_BUILD_CACHE/bitbake"
    - !reference [.workspace, before_script]

clean_sstate:
  extends: .bitbake-workspace
  script:
    - SCRATCH_DIR="$(cat "$CI_PROJECT_DIR"/.scratch-dir-name)"
    - cd "$SCRATCH_DIR"
    - df -h ${CI_ONIRO_RUNNER_PERSISTENT_STORAGE}
    - time ${SCRATCH_DIR}/oe-core/scripts/sstate-cache-management.sh --remove-duplicated -d --cache-dir=${CI_ONIRO_BB_LOCAL_CONF_SSTATE_DIR} -y
    - df -h ${CI_ONIRO_RUNNER_PERSISTENT_STORAGE}

